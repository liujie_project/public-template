// element-ui 按需加载
import Vue from 'vue';
import {
  Input,
  Form,
  FormItem,
  Button,
  ButtonGroup,
  Tree,
} from 'element-ui';
Vue.use(Input);
Vue.use(Form);
Vue.use(FormItem);
Vue.use(Button);
Vue.use(ButtonGroup);
Vue.use(Tree);