/**
 * 权限对比
 * @param matchArr
 * @param userArr
 * @returns {boolean}
 */
 export function authorityComparison(matchArr, userArr) {
  if (!userArr) {
    // 如果没有设置权限码，默认不需要
    return true;
  }
  let temp = false;
  matchArr = matchArr || [];
  // console.log('所有权限码matchArr',matchArr)
  console.log('当前页面权限码userArr',userArr)
  if (typeof userArr === 'string' || typeof userArr === 'number') {
    return matchArr.includes(userArr);
  } else {
    userArr.forEach(item => {
      temp = temp || matchArr.includes(item);
    });
  }
  return temp;
}