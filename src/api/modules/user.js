import $axios from '@/service/httpServer';

// 登陆
export const loginByToken = () => $axios.get('/auth/user/token/details');
// 登陆
export const login = p => $axios.postFormData('/auth/oauth/token', p, { Authorization: 'Basic dXBtczp1cG1z' });