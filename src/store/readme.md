store 使用方式
  state:      定义变量
  actions:    暴露方法
  mutations:  变量赋值


使用

  // 赋值
  watch: {
    'value': {
      handler() {
        if (this.value) {
          // 将值 设置到store中
          store.dispatch('setValue', this.value);
        }
      },
      immediate: true, // 其值是true或false；确认是否以当前的初始值执行handler的函数。
      deep: true, // 其值是true或false；确认是否深入监听。
    },
  },

  //取值
  let value = this.$store.state.value;