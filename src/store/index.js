import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);
/**
 * 各个模块
 */

export default new Vuex.Store({
  state: {
    name: 'partner-manage',
    isLogin: false,
    hasLoadPermission: false,
    value: "",
  },
  getters:{

  },
  setters:{
    
  },
  // 行为
  actions: {
    /**
     *  获取个人权限
     */
    getIsLogin({
      commit,
      state
    }) {
      return new Promise((resolve, reject) => {
        resolve(state.isLogin);
      });
    },
    setValue({
      commit
    }, val) {
      commit('SET_VALUE', val)
    }
  },
  // 
  mutations: {
    SET_VALUE(state, val) {
      state.value = val
    },
    /**
     * 是否登录
     * @param state
     * @param flag
     */
    UPDATE_IS_LOGIN(state, flag) {
      state.isLogin = flag;
    }
  },
  modules: {}
});