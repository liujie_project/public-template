import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '../store/index'
import userModel from '@/common/js/userModel'

const routes = [{
    path: "/",
    name: "Layout",
    component: () => import('@/views/page-router.vue'),
    redirect: {
      name: "Home"
    },
    children: [{
        path: "home",
        name: "Home",
        component: () => import('@/views/Home.vue'),
        meta: {
          title: '首页',
        }
      },
      {
        path: "module-one",
        name: "ModuleOne",
        component: () => import('@/views/moduleOne/index.vue'),
        meta: {
          title: '模块1',
        }
      },
      {
        path: "module-two",
        name: "ModuleTwo",
        component: () => import('@/views/moduleTwo/index.vue'),
        meta: {
          title: '模块2',
          noNeedLogin: true
        }
      },
      {
        path: "canvas",
        name: "Canvas",
        component: () => import('@/views/canvas/index.vue'),
        meta: {
          title: 'Canvas',
          noNeedLogin: true
        }
      },
      {
        path: "iframe",
        name: "Iframe",
        component: () => import('@/views/iframe/index.vue'),
        meta: {
          title: 'Iframe',
          noNeedLogin: true
        }
      },
      {
        path: "tree",
        name: "Tree",
        component: () => import('@/views/tree/index.vue'),
        meta: {
          title: 'Tree',
          noNeedLogin: true
        }
      },
      {
        path: "countDown",
        name: "CountDown",
        component: () => import('@/views/countDown/index.vue'),
        meta: {
          title: '倒计时',
          noNeedLogin: true
        }
      },
      {
        path: "barCode",
        name: "BarCode",
        component: () => import('@/views/jsBarCode/index.vue'),
        meta: {
          title: '条形码',
          noNeedLogin: true
        }
      },
      {
        path: "qrCode",
        name: "QrCode",
        component: () => import('@/views/qrCode/index.vue'),
        meta: {
          title: '二维码',
          noNeedLogin: true
        }
      },
      {
        path: "vueDraggable",
        name: "VueDraggable",
        component: () => import('@/views/vueDraggable/index.vue'),
        meta: {
          title: '拖拽',
          noNeedLogin: true
        }
      },
      {
        path: "observer",
        name: "Observer",
        component: () => import('@/views/observer/index.vue'),
        meta: {
          title: '观察者模式',
          noNeedLogin: true
        }
      }
    ]
  },
  {
    path: "/login",
    name: "Login",
    component: () => import('@/views/login.vue'),
  }
]


let router = new VueRouter({ routes })
Vue.use(VueRouter);

router.beforeEach(async (to, from, next) => {
  let loginStatus = await loginStatusInterceptors(to);
  if (!loginStatus) {
    next({ name: "Login" })
  } else {
    next();
  }
});

async function loginStatusInterceptors(to) {
  if(to.path === '/login'){
    return true;
  }
  // to.meta.noNeedLogin router 配置参数，是否需要登录校验
  let loginStatus = true;
  if (!userModel.isLogin() && !to.meta.noNeedLogin) {
    loginStatus = false;
  } else {
    store.commit('UPDATE_IS_LOGIN', true);
  }
  return loginStatus;
}
export default router