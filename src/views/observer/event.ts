import { Class, NotificationCenter, Event } from "@/nc";
let nc_ = new NotificationCenter();
export class ValueChangedEvent extends Event {
  public constructor() {
    super();
    console.log("========ValueChangedEvent===========");
  }
}

export class RegisterEvent {
  public constructor(eventName: Class<Event>, callback: Function) {
    console.log("========RegisterEvent===========");
    nc_.register(this, eventName, async (event) => {
      callback(event);
    });
  }
}
export class PostEvent {
  public constructor(event: Event) {
    console.log("========PostEvent===========");
    nc_.post(event);
  }
}
