// 普通 下载
export function fileDownload(url, name) {
  let link = document.createElement('a');
  link.setAttribute('download', name);
  link.href = url;
  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);
}
// iframe 实现多个文件下载
export function iframeDownload(url) {
  try {
    let elemIF = document.createElement('iframe');
    elemIF.src = url;
    elemIF.style.display = 'none';
    document.body.appendChild(elemIF);
  } catch (e) {
    console.error('下载出错了。。。。', e);
  }
}

// 文本文件下载
export function txtFileDownload(fileUrl, fileName = "") {
  let xhr = new XMLHttpRequest();
  xhr.open("get", fileUrl, true);
  xhr.responseType = "blob";
  xhr.onload = function () {
    if (this.status == 200) {
      const reader = new FileReader()
      reader.onload = function () {
        let link = document.createElement('a');
        link.setAttribute(
          "href",
          "data:text/plain;charset=utf-8," + encodeURIComponent(reader.result)
        );
        link.setAttribute('download', fileName);
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
      reader.readAsText(this.response);
    }
  };
  xhr.send();
}

// pdf、mp4文件下载
export function pdfDownload(fileUrl, fileName = "") {
  let xhr = new XMLHttpRequest();
  xhr.open("get", fileUrl, true);
  xhr.responseType = "blob";
  xhr.onload = function () {
    if (this.status == 200) {
      let res = this.response;
      // 创建blob对象，解析流数据
      const blob = new Blob([res], {
        type: res.type
      })
      const link = document.createElement('a');
      // 兼容webkix浏览器，处理webkit浏览器中href自动添加blob前缀，默认在浏览器打开而不是下载
      const URL = window.URL || window.webkitURL
      // 根据解析后的blob对象创建URL 对象
      const fileUrl = URL.createObjectURL(blob);
      // 下载文件名,如果后端没有返回，可以自己写a.download = '文件.pdf'
      link.setAttribute('download', fileName);
      // 下载链接
      link.href = fileUrl;
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
      // 在内存中移除URL 对象
      window.URL.revokeObjectURL(fileUrl);
    }
  };
  xhr.send();
}


// 文件类型判断
export function fileIconFormat(url) {
  if (/\.(docx?)$/i.test(url)) {
    return 'doc.png';
  } else if (/\.(pptx?|ppsx?|ppam?)$/i.test(url)) {
    return 'ppt.png';
  } else if (/\.(pdf)$/i.test(url)) {
    return 'pdf.png';
  } else if (/\.(xlsx?)$/i.test(url)) {
    return 'excel.png';
  } else if (/\.(txt)$/i.test(url)) {
    return 'txt.png';
  } else if (/\.(psd)$/i.test(url)) {
    return 'psd.png';
  } else if (/\.(ai)$/i.test(url)) {
    return 'ai.png';
  } else {
    return 'file.png';
  }
}