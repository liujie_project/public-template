const fs = require('fs');

/**
 * 写文件
 * @param {*} path 文件路径
 * @param {*} buffer 文件内容
 * @param {*} callback 
 */
export function writeFileRecursive(path, buffer, callback) {
  console.log('fs=', fs);
  let lastPath = path.substring(0, path.lastIndexOf("/"));
  fs.mkdir(lastPath, { recursive: true }, (err) => {
    if (err) return callback(err);
    fs.writeFile(path, buffer, function (err) {
      if (err) return callback(err);
      return callback(null);
    });
  });
}
