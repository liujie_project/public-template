import store from '@/store';
import router from '@/router'
import {
  login
} from '@/api';
import {
  getLocalStorage,
  setLocalStorage
} from './mUtils';

let userModel = {
  async doLogin(options) {
    // if (
    //   options.username === "admin" &&
    //   options.password === "123456"
    // ) {
    //   // 设置状态
    //   // todo 调用登录接口获取token
    //   let code = '123456';
    //   setLocalStorage('user_token', JSON.stringify({ code }));
    //   store.commit("UPDATE_IS_LOGIN", true);
    //   router.push({ name: 'Home' });
    // }
    console.log('------------doLogin--------------');
    let params = {
      grant_type: 'authorization_code',
      code: 'phM2uF'
    }
    login(params)
      .then(res => {
        // store.commit('UPDATE_OAUTH', res.body);
        console.log('------------login--------------');
        resolve(res.body);
      })
  },

  async getUserToken() {
    return getLocalStorage('user_token');
  },

  async isLogin() {
    let loginStatus = store.state.isLogin;
    if (!loginStatus) {
      store.commit("UPDATE_IS_LOGIN", true);
      loginStatus = userModel.getUserToken() ? true : false;
    }
    return loginStatus
  }
}

export default userModel