/**
 * 存储localStorage
 */
 export const setLocalStorage = (name, content) => {
  if (!name) return;
  if (typeof content !== 'string') {
    content = JSON.stringify(content);
  }
  window.localStorage.setItem(name, content);
};

/**
 * 获取localStorage
 */
export const getLocalStorage = name => {
  if (!name) return;
  let data = window.localStorage.getItem(name);

  return data ? JSON.parse(data) : undefined;
};

/**
 * 删除localStorage
 */
export const removeLocalStorage = name => {
  if (!name) return;
  window.localStorage.removeItem(name);
};

/**
 * Cookie操作
 * @type {{get(*): *, set(*, *, *=, *=, *=): void, remove(*=): void}}
 */
export const Cookie = {
  /**
   * getCookie
   * @param name
   * @returns {*}
   */
  get(name) {
    var strCookie = document.cookie;
    var arrCookie = strCookie.split('; ');
    for (var i = 0; i < arrCookie.length; i++) {
      var arr = arrCookie[i].split('=');
      if (arr[0] == name) return arr[1];
    }
    return '';
  },

  /**
   * 添加cookie
   * @param name
   * @param value
   * @param expiresHours
   * @param domain
   */
  set(name, value, expiresDays, domain, path) {
    var cookieString = name + '=' + (value ? value : '');
    var date = new Date();

    if (domain != undefined) domain = ';domain=' + domain;
    else domain = '';

    date.setTime(date.getTime() + (expiresDays || 1) * 24 * 3600 * 1000);
    cookieString =
      cookieString +
      domain +
      '; path=' +
      (path || '/') +
      '; expires=' +
      date.toGMTString();
    document.cookie = cookieString;
  },

  /**
   * 删除cookie
   * @param name
   */
  remove(name) {
    this.set(name, '', -1);
  }
};


 

export const urlDelParams = (url,name) =>{
	var urlArr = url.split('?');
	if(urlArr.length>1){
		var query = urlArr[1];
		var obj = {}
		var arr = query.split("&");
		for (let i = 0; i < arr.length; i++) {
			arr[i] = arr[i].split("=");
			obj[arr[i][0]] = arr[i][1];
		}
		if(typeof name === 'string'){
			delete obj[name];
		}else{
			for (let i = 0; i < name.length; i++) {
				delete obj[name[i]];
			}
		}
		var urlte = urlArr[0] +'?'+ JSON.stringify(obj).replace(/[\"\{\}]/g, "").replace(/\:/g, "=").replace(/\,/g, "&");
		return urlte+'&timeDump='+new Date().getTime()+'T';
	}else{
		return url+'&timeDump='+new Date().getTime()+'T';
	}
}

// 数据非空判断
export const isEmpty=(value)=>{
	if(value instanceof Array && !value.length === 0){
		return true
	}
	if(typeof value === 'number'){
		return false
	}
	if(typeof value === 'string'){
		return !value
	}
	if(typeof value === 'object'){
		return Object.keys(value).length === 0 ? true : false
	}
	return false
}

/**
 *  删除对象中值为空的属性
 * 
 * @param {Object} params 需要删除的对象
 * 
 */
 export function deleteEmptyParams(params) {
  const keys = Object.keys(params);
  keys.map((key) => {
    if (!params[key]) {
      delete params[key];
    }
  });
}


/**
 * 获取当前的日期 格式“yyyy-MM-dd” hh:mm:ss
 * @returns {string}
 */
 export function getcurrentDateAll(nS) {
  var date = nS ? new Date(nS) : new Date();
  var year = date.getFullYear();
  var mon = date.getMonth() + 1;
  var day = date.getDate();
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var seconds = date.getSeconds();
  mon = mon > 9 ? mon : '0' + mon;
  day = day > 9 ? day : '0' + day;
  hours = hours > 9 ? hours : '0' + hours;
  minutes = minutes > 9 ? minutes : '0' + minutes;
  seconds = seconds > 9 ? seconds : '0' + seconds;
  return '' + year + '-' + mon + '-' + day + ' ' + hours + ':' + minutes + ':' + seconds;
}


// 时间戳转换为 xx天xx小时xx分钟xx秒
export function dateForm(timestamp) {
  if (!timestamp) return;
  timestamp = Number(timestamp);
  let second = Math.ceil(timestamp / 1000);
  let minute = 0;
  let hour = 0;
  let day = 0;
  let str = "";
  if (second >= 60) {
    minute = Math.floor(second / 60);
  }
  if (minute >= 60) {
    hour = Math.floor(minute / 60);
  }
  if (hour >= 24) {
    day = Math.floor(hour / 24);
  }
  day = day === 0 ? `` : `${day}天`;
  hour = hour % 24 === 0 ? `` : `${hour % 24}小时`;
  minute = minute % 60 === 0 ? `` : `${minute % 60}分钟`;
  second = second % 60 === 0 ? `` : `${second % 60}秒`;
  str = `${day}${hour}${minute}${second}`;
  return str;
}

/**
 * 防抖函数
 * @param {*} func 传入方法
 * @param {*} delay 延迟时间
 */
 export function debounce(func, delay) {
  let timer
  return function (...args) {
    if (timer) {
      clearTimeout(timer)
    }
    timer = setTimeout(() => {
      func.apply(this, args)
    }, delay)
  }
}


/**
 * 解决科学计数
 */
 export function toolNumber(num_str) {
  num_str = num_str.toString();
  if (num_str.indexOf("+") != -1) {
    num_str = num_str.replace("+", "");
  }
  if (num_str.indexOf("E") != -1 || num_str.indexOf("e") != -1) {
    var resValue = "",
      power = "",
      result = null,
      dotIndex = 0,
      resArr = [],
      sym = "";
    var numStr = num_str.toString();
    if (numStr[0] == "-") {
      // 如果为负数，转成正数处理，先去掉‘-’号，并保存‘-’.
      numStr = numStr.substr(1);
      sym = "-";
    }
    if (numStr.indexOf("E") != -1 || numStr.indexOf("e") != -1) {
      var regExp = new RegExp(
        "^(((\\d+.?\\d+)|(\\d+))[Ee]{1}((-(\\d+))|(\\d+)))$",
        "ig"
      );
      result = regExp.exec(numStr);
      if (result != null) {
        resValue = result[2];
        power = result[5];
        result = null;
      }
      if (!resValue && !power) {
        return false;
      }
      dotIndex = resValue.indexOf(".") == -1 ? 0 : resValue.indexOf(".");
      resValue = resValue.replace(".", "");
      resArr = resValue.split("");
      if (Number(power) >= 0) {
        var subres = resValue.substr(dotIndex);
        power = Number(power);
        //幂数大于小数点后面的数字位数时，后面加0
        for (var i = 0; i <= power - subres.length; i++) {
          resArr.push("0");
        }
        if (power - subres.length < 0) {
          resArr.splice(dotIndex + power, 0, ".");
        }
      } else {
        power = power.replace("-", "");
        power = Number(power);
        //幂数大于等于 小数点的index位置, 前面加0
        for (var i = 0; i < power - dotIndex; i++) {
          resArr.unshift("0");
        }
        var n = power - dotIndex >= 0 ? 1 : -(power - dotIndex);
        resArr.splice(n, 0, ".");
      }
    }
    resValue = resArr.join("");

    return sym + resValue;
  } else {
    return num_str;
  }
}




/**
 * tree数据格式转换为list 
 * @param treeData Tree 原tree数据
 * @param dataList Array 转换后的 list 数据
 * @param idList Array 转换后的 id 数据
 */
export function treeToList(treeData, dataList, idList, parentId=null){
  treeData.forEach(tree=>{
    // 拷贝当前对象，删除其 children 属性
    let obj = Object.assign({},tree)
    delete obj.children
    if(!obj.parentId) obj["parentId"] = parentId
    dataList.push(obj)
    idList.push({ id: obj.id, parentId: parentId })
    if(!tree.children || tree.children.length===0) return
    treeToList(tree.children, dataList, idList, tree.id)
  })
}

/**
 * 
 * @param {*} treeData 
 * @param {*} categoryId 
 * @returns 根据 categoryId 匹配 treeData的数据，返回对应的层级 label
 */
export function getCategoryLevelLabels(treeData,categoryId){
  const idList = []
  const treeList = []
  let levelTitle = []
  treeToList(treeData, treeList, idList)
  levelTitle.push(getLabel(treeList, categoryId))
  recursion(idList, treeList, categoryId, levelTitle)
  return levelTitle.join("》")
}
// 递归获取每层的 label
function recursion(idList, treeList, id, levelTitle){
  const parentId = getId(idList, id)
  if(!parentId) return
  levelTitle.unshift(getLabel(treeList, parentId))
  recursion( idList, treeList, parentId, levelTitle )
}
function getId(idList, id){
  return idList.find(item =>item.id === id).parentId
}
function getLabel(treeList, id){
  return treeList.find(tree =>tree.id === id).label
}