export const BaseUrl = "https://fatxwyapi.newhope.cn";

export const BranchName = "feature-0.9";

/**
 * 配置文件
 */
export const configObj = {
  // 区分环境配置
  baseURL: process.env.VUE_APP_BASE_URL,

  // 公共页面配置
  paginationSize: 20, // 页面表格每页显示条数
  paginationPages: [10, 20, 30, 40, 60, 80, 100],
  version: "1.0.0",
};
