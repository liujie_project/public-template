import axios from 'axios';
import store from '@/store/index';
import $config from '@/config';
import QS from 'qs';

// 线上环境配置axios.defaults.baseURL，生产环境则用proxy代理
console.log('process==========', process);
if (process.env.NODE_ENV !== 'development') {
  axios.defaults.baseURL = $config.baseURL;
}


//请求拦截器
axios.interceptors.request.use(
  config => {
    config.headers.appId = '';
    config.headers.remote_user = '';
    config.headers.version ='';
    config.headers.Authorization = 'Basic dXBtczp1cG1z';
    config.headers['Access-Control-Allow-Origin'] = '*';
    return config;
  },
  error => {
    return Promise.reject(error);
  }
);


//响应拦截器即异常处理
axios.interceptors.response.use(
  response => {
    console.log('response=', response);
    if (response.data.status) {
      return Promise.resolve(response.data);
    } else {
      store.dispatch('showMessage', {
        type: 'error',
        message:
          response.data.message || response.data.msg || response.data.errMsg
      });
      return Promise.reject(
        response.data.message || response.data.msg || response.data.errMsg,
        response
      );
    }
  },
  err => {
    console.log('err=', err);
    if (err && err.response) {
      switch (err.response.status) {
        case 400:
          err.message = '错误请求';
          break;
        case 401:
          err.message = '未授权，请重新登录';
          break;
        case 403:
          err.message = '没有访问权限，拒绝访问';
          break;
        case 404:
          err.message = '请求错误,未找到该资源';
          break;
        case 405:
          err.message = '请求方法未允许';
          break;
        case 500:
          err.message = err.response.data.message || '服务器端出错';
          break;
        default:
          err.message = `连接错误${err.response.msg}`;
      }
    } else {
      err.message = '连接到服务器失败';
    }
    if (err.response.status !== 401) {
      store.dispatch('showMessage', {
        type: 'error',
        message: err.message || err.response.msg
      });
    }
    return Promise.reject(err.response, err);
  }
);

/**
 * @description: 下载文件
 * @param {*}
 * @return {*}
 */
 let downloadFile = (url) => {
  window.open(url);
};

export default {
  //get请求
  get(url, param, header) {
    return axios({
      method: 'get',
      url,
      headers: {
        ...(header || {})
      },
      params: param || {}
    });
  },
  //post请求
  post(url, param, header, options) {
    return axios({
      method: 'post',
      url,
      headers: {
        ...(header || {}),
        'Content-Type': 'application/json;charse=UTF-8'
      },
      data: param || {},
      ...(options || {})
    });
  },
  //post请求
  put(url, param, header) {
    return axios({
      method: 'put',
      url,
      headers: {
        ...(header || {}),
        'Content-Type': 'application/json;charse=UTF-8'
      },
      data: param || {}
    });
  },
  // delete
  delete(url, param, header) {
    return axios({
      method: 'delete',
      url,
      headers: {
        ...(header || {})
      },
      params: param || {}
    });
  },
  //post请求
  postFormData(url, param, header) {
    return axios({
      method: 'post',
      url,
      headers: {
        ...(header || {}),
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      data: QS.stringify(param) || {}
    });
  },
  uploadFile(url, params, header) {
    let data = new FormData();
    for (let key in params) {
      data.append(key, params[key]);
    }
    return axios({
      method: 'post',
      url: `${url}`,
      data: data,
      headers: {
        ...header,
        'Content-Type': 'multipart/form-data'
      }
    });
  },
  // 此方法非promise 导出文件
  getFile(url, params) {
    let token = store.getters.authorization;
    token = token.replace('Bearer ', '');
    let tempParams = {
      ...(params || {}),
      token: token
    };
    // 拼接下载地址
    let list = [];
    for (let key in tempParams) {
      list.push(key.toString() + '=' + tempParams[key]);
    }
    url = /^http(s?):\/\//.test(url) ? url : $config.baseURL + url;
    url = list.length ? url + '?' + list.join('&') : url;
    url = encodeURI(url);
    downloadFile(url);
  }
  
};