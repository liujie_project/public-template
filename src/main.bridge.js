import Vue from 'vue';
import App from './App'
import store from './store';
import router from './router';
import * as mUtils from '@/common/js/mUtils';
import * as API from '@/api/index';
// element-ui 按需加载
require("./elementUI.js");
 
// import echarts from 'echarts'
// Vue.prototype.$echarts = echarts;

// import VueClipboard from 'vue-clipboard2'
// Vue.use(VueClipboard)

// 权限控制方法
// import permission from '@/permission';
Vue.prototype.comparePermissionsHopeManage = userPermission => {
  if (!userPermission) {
    return true;
  }
  return mUtils.authorityComparison(
    store.state.permissionsList,
    userPermission
  );
};
Vue.prototype.$mUtils = mUtils;
// 全局api
Vue.prototype.$api = API;
 
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');

export default { router };