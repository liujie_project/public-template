common/js 
  fileDownload -- 文件下载
  loadScript   -- 动态加载 依赖包
  mUtils       -- 常用工具方法
  txtUtils     -- 文本文件读写
  userModel    -- 用户数据



views
  canvas      -- canvas 动画
  countDown   -- 倒计时
  iframe      -- iframe 父子组件事件监听
  jsBarCode   -- 条形码
  navs        -- 菜单栏
  qrCode      -- 二维码
  tree        -- 高亮显示搜索的关键字




https://gitlab.com/liujie_project/public-template/-/tree/feature-0.0.1

https://gitlab.com/liujie_project/public-template.git