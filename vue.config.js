// const webpack = require('webpack');
const path = require('path');
// const fs = require('fs');
// const APP_NAME = require('./package.json').name;
const PORT = require('./package.json').port;
const CompressionPlugin = require('compression-webpack-plugin')
const PROXYTABLE = require('./proxyTable');

module.exports = {
	pages: {
		index: {
      // 修改这，以前是 main.js  修改成 main.ts
			entry: 'src/main.ts', // 入口
			template: 'public/index.html', // 模板
			filename: 'index.html' // 输出文件
		}
  },
	// publicPath: `${process.env.NODE_ENV === 'development' ? '' : '.'}/${APP_NAME}/`,
	// outputDir: `./dist/${APP_NAME}`,
	publicPath: `${process.env.NODE_ENV === 'development' ? '' : '.'}/`,
	outputDir: `./dist`,
	productionSourceMap: process.env.VUE_APP_ENV_NAME === 'development' || process.env.VUE_APP_ENV_NAME === 'test',
	css: {
		loaderOptions: {
			sass: {
				// @/ 是 src/ 的别名
				// prependData: fs.readFileSync(path.resolve(__dirname, `./src/common/styles/var.scss`), 'utf-8') // 公共变量文件注入
			}
		}
	},
	// webpack config
	configureWebpack: {
		// 扩展别名
		resolve: {
			alias: {
				'@': path.resolve(__dirname, `./src`),
				'assets': path.resolve(__dirname, `./src/assets`),
				'common': path.resolve(__dirname, `./src/common`),
				'components': path.resolve(__dirname, `./src/components`),
				'views': path.resolve(__dirname, `./src/views`),
				'utils': path.resolve(__dirname, `./src/utils`),
				'styles': path.resolve(__dirname, `./src/styles`),
				'api': path.resolve(__dirname, `./src/api`),
				'router': path.resolve(__dirname, `./src/router`),
				'store': path.resolve(__dirname, `./src/store`),
			}
		},
		externals: {
			// vue: 'Vue',
			// 'vue-router': 'VueRouter',
			// vuex: 'Vuex',
			// axios: 'Axios',
			// 'element-ui': 'Element',
			// 'nh-components': 'NhComponents'
		}
	},
	chainWebpack: (config) => {

		config.output
			.filename('main.js')
			.chunkFilename('[name].[chunkhash:8].js')
			// .jsonpFunction(`webpackJsonp-${APP_NAME}`)
			// .library(`app-${APP_NAME}`)
			.libraryExport('default')
			.libraryTarget('umd')

		config.optimization.splitChunks(false)

		// 移除插件
		config.plugins
			.delete('html')
			.delete('preload')
			.delete('prefetch')

		// gzip做一下压缩
		if (process.env.NODE_ENV === 'production') {
			return {
				plugins: [new CompressionPlugin({
					test: /\.js$|\.html$|\.css|\.png$|\.jpg$/, //匹配文件名
					threshold: 10240, //对超过10k的数据进行压缩
					deleteOriginalAssets: false //是否删除原文件
				})]
			}
		}
	},

	// 接口代理
	devServer: {
		port: PORT,
		proxy: PROXYTABLE
	}
}
