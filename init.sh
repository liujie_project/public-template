#!/usr/bin/env bash


if [ $# -ne 1 ] || ( [ "x$1" != 'xdev' ]  && [ "x$1" != 'xpro' ] && [ "x$1" != 'xuat' ] && [ "x$1" != 'xfat' ]) ;then
	echo "Usage: $0 dev/fat/uat/pro" >&2
	exit 1
fi

echo "init $1 ..." ;\
cp ./config/config.$1.ts ./src/config.ts

BranchName="export const BranchName = \""`git rev-parse --abbrev-ref HEAD`"\";"

echo -e "\n"${BranchName} >> ./src/config.ts
