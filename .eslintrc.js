/* eslint-disable */
module.exports = {
  root: true,
  env: {
    node: true
  },
  'extends': [
    'plugin:vue/essential',
    '@vue/typescript'
  ],
  parserOptions: {
    parser: '@typescript-eslint/parser'
  },
  rules: {
	  'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
	  "no-console": 1,
	  "no-undef": 1,
	  "no-empty": 1,
	  "no-unused-vars": 1,
	  "vue/no-unused-vars": 1,
	  "vue/no-use-v-if-with-v-for": 1,
	  "no-irregular-whitespace": 1,
    "no-mixed-spaces-and-tabs": 1,
    "no-useless-escape":0
  }
}
